/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import Layout from './views/Layout.vue'
import Signup from './views/Signup.vue'
import Signin from './views/Signin.vue'
import HomePage from './views/HomePage.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/calendar',
      name: 'Layout',
      component: Layout
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin
    },
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    },
    
  ]
})
