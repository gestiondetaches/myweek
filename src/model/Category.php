<?php

namespace myweek\model;

class Category extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'categories';
    protected $primaryKey = 'categoryId';
    public $timestamps = false;
    protected $fillable = ['name','color','toggle'];

    public function user(){
        return $this->belongsTo('myweek\model\User','userId');
    }

    public function tasks(){
        return $this->hasMany('myweek\model\Task', 'categoryId');
    }
}