<?php

namespace myweek\model;

class Subtask extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'subtasks';
    protected $primaryKey = 'subtaskId';
    public $timestamps = false;

    public function task(){
        return $this->belongsTo('myweek\model\Task', 'taskId');
    }
}