<?php
/* Error / Response */
use myweek\response\Writter;

return [
    /* Configuration bdd et slim */
    'settings' => ['displayErrorDetails' => true,
                   'dbconf' => __DIR__ . '/config.ini'],

    /* Réécriture des erreurs slim */
    'notFoundHandler' => function ($container){
        return function ($rq, $rs){
            return \myweek\errors\NotFound::error($rq, $rs);

        };
    },
    'notAllowedHandler' => function ($container){
        return function ($rq, $rs,$methods){
            return \myweek\errors\NotAllowed::error($rq,$rs,$methods);
        };
    },

    'phpErrorHandler' => function ($container){
        return function ($rq, $rs,$error){
            return \myweek\errors\PhpError::error($rq,$rs,$error);
        };
    },
    'errorHandler' => function( $container ) {
        return function ($rq, $rs, Exception $previousException, $message = '', $code = 400) {
            if (empty($message)) {
                $message = "Erreur lors de l'execution " . $previousException->getMessage();
            }
            return Writter::jsonerror($rs, $message, $code);
        };
    }

];
