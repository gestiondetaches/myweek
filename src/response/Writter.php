<?php

namespace myweek\response;

/* Slim */
use Slim\App;
use Slim\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Writter{

    /** Méthode jsonError
     * 
     * Renvoit une erreur json
     *
     * @param  mixed $resp
     * @param  mixed $message
     * @param  mixed $code default 404
     *
     * @return Response
     */
    public static function jsonError($resp, $message, $code = 404) {
        $body = [
            "type" => "error",
            "error" => $code,
            "message" => $message
        ];

        $resp = $resp->withStatus($code)->withHeader('Content-Type', 'application/json;charset=utf-8');
        $respJson = json_encode($body);
        $resp->write("$respJson");
        return $resp;
    }

    /** Méthode jsonSuccess
     *
     * Renvoit les données au format json
     * 
     * @param  mixed $response
     * @param  mixed $result
     * @param  mixed $code
     * @param  mixed $type
     *
     * @return Response
     */
    public static function jsonSuccess($response, $result, $code = 200, $type = 'resource') {
        $result["locale"] = "fr-FR";
        $result["type"] = $type;
        $result = json_encode($result);
        $response->write("$result");

        return $response->withStatus($code)->withHeader('Content-Type', 'application/json;charset=utf-8');
    }

}