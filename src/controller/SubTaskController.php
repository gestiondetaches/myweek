<?php

namespace myweek\controller;

/* Slim */
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/* Model */
use myweek\model\Subtask;

/* Errors / Response */
use myweek\response\Writter;
use myweek\errors\PhpError;

class SubTaskController
{
    protected $app;

    public function __construct($pApp)
    {
        $this->app = $pApp;
    }

    /** Méthode index
     *
     * Récupère les taches de l'utilisateur
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return PhpError|Response
     */
    public function index(Request $request, Response $response, array $args)
    {
        $params = $request->getQueryParams();
        $idTask = $params['idTask'];
        if (!empty($idTask)) {
            $subtasks = Subtask::where('taskId', '=', $idTask)
                ->orderBy('title', 'ASC')
                ->get();

            foreach ($subtasks as $subtask) {
                $subtask->task;
            }
            $data['Subtasks'] = $subtasks;
            return Writter::jsonSuccess($response, $data, 200, 'collection');

        } else {
            return Writter::jsonError($response, "Id de la tache manquant", 403);
        }
    }

    /** Méthode create
     *
     * Ajoute une tache à l'agenda de l'utilisateur
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return PhpError|Response
     */
    public function create(Request $request, Response $response, array $args)
    {
        $body = $request->getParsedBody();
        $title = "";
        $taskId = "";
        $error = false;

        if (filter_var($body['title'], FILTER_SANITIZE_STRING)) {
            $title = $body['title'];
        } else {
            $error = true;
        }
        if (filter_var($body['taskId'], FILTER_SANITIZE_STRING)) {
            $taskId = $body['taskId'];
        } else {
            $error = true;
        }

        if (!$error) {
            try {
                $newSubTask = new Subtask();
                $newSubTask->title = $title;
                $newSubTask->taskId = $taskId;
                $newSubTask->save();

                return Writter::jsonSuccess($response, array('success' => 1), 204);
            } catch (\Exception $e) {
                return PhpError::error($request, $response, "Erreur dans la programmation");
            }
        } else {
            return Writter::jsonError($response, "Données manquantes", 403);
        }
    }

    /** Méthode edit
     *
     * Modifie en partie une sous tache (pour l'instant juste le check) (PATCH)
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function edit(Request $request, Response $response, array $args): Response
    {
        if (isset($args['id'])) {
            $id = $args['id'];
            $subtask = Subtask::where("subtaskId", "=", $id)->first();

            if (!empty($subtask)) {
                $body = $request->getParsedBody();

                if (isset($body['check'])) {
                    $subtask->check = $body['check'];
                }

                try {
                    $subtask->save();
                    return Writter::jsonSuccess($response, array('success' => 1), 204);
                } catch (\Exception $e) {
                    PhpError::error($request, $response, "Erreur dans la prgrammation");
                }
            } else {
                return Writter::jsonError($response, "Sous-Tache inconnue", 404);
            }
        } else {
            return Writter::jsonError($response, "Id est manquant", 403);
        }
    }

    /** Méthode delete
     *
     * Supprime une tache dans l'agenda de l'utilisateur
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return PhpError|Response
     */
    public function delete(Request $request, Response $response, array $args)
    {
        if (isset($args['id'])) {
            $id = $args['id'];
            $subtask = Subtask::where("subtaskId", "=", $id)->first();

            if (!empty($subtask)) {
                try {
                    $subtask->delete();
                    return Writter::jsonSuccess($response, array('success' => 1), 204);
                } catch (\Exception $e) {
                    return PhpError::error($request, $response, "Erreur dans la programmation");
                }
            } else {
                return Writter::jsonError($response, "La sous-tâche est incunnue", 404);
            }
        } else {
            return Writter::jsonError($response, "Id est manquant", 403);
        }
    }
}