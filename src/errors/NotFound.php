<?php

namespace myweek\errors;

/* Slim */
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/* Error / Response */
use myweek\response\Writter;

class NotFound{

    /** Méthode error
     * Renvoit une erreur 404
     * 
     * @param  mixed $rq
     * @param  mixed $rs
     *
     * @return Response
     */
    public static function error(Request $rq, Response $rs){
            return Writter::jsonError($rs, $rq->getUri());
    }

}