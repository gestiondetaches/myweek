# Routes de l'application MyWeek

| Route         	    | Method     | Header Params  			    | Body Params 									    | Description 				                 |
| ----------------------|:----------:| ----------------------------:|--------------------------------------------------:|-------------------------------------------:|
| /ping[/]    	        | GET        | none                         | none    										    | Vérifie la connection à l'API              |
| /signup[/]    	    | POST       | none                         | firstName, lastName, username, email, password, verifPassword | Création d'un nouveau compte   |
| /login[/]    	        | POST       | Auth Basic                   | none    										    | Connection à l'application                 |
| /logout[/]    	    | DELETE     | Auth Bearer                  | none    										    | Déconnection à l'application               |
| /users/search[/]    	| GET        | Auth Bearer + ?param={name}  | none    										    | Liste des utilisateurs correspondant au nom dans le param |
| /tasks[/]    	        | GET        | Auth Bearer                  | none    										    | Liste des tâches de l'utilisateurs |
| /tasks[/]    	        | POST       | Auth Bearer                  | title, description, categoryId, priority, startDate (YYYY-MM-DD), endDate (YYYY-MM-DD), subtasks (tableau de nom), usersId (tableau d'id)| Ajoute une tâche |
| /tasks/{id}[/]    	| PATCH       | Auth Bearer                 | title, description, categoryId, priority, startDate (YYYY-MM-DD), endDate (YYYY-MM-DD), subtasks (tableau de nom), usersId (tableau d'id)| Modifie une tâche |
| /tasks/{id}[/]    	| DELETE      | Auth Bearer                 | none                                              | Supprime une tâche                         |
| /profiles/{id}[/]    	| GET         | Auth Bearer                 | none                                              | Donne le détail d'un utilisateur           |
| /profiles/{id}[/]    	| PATCH       | Auth Bearer                 | firstName, lastName, username, email, password, verifPassword | Donne le détail d'un utilisateur |
| /categories[/]    	| GET         | Auth Bearer                 | none                                              | Liste des categories de l'utilisateur      |
| /categories[/]    	| POST        | Auth Bearer                 | name, color                                       | Ajoute une categorie                       |
| /categories[/]    	| PATCH       | Auth Bearer                 | name, color                                       | Modifie une categorie                      |
| /categories[/]    	| DELETE      | Auth Bearer                 | name                                              | Supprime une categorie                     |
